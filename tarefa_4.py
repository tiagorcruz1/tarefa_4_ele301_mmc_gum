# -*- coding: utf-8 -*-
"""
Created on Tue Apr 30 15:53:04 2019

@author: Tiago Cruz
"""

from math import sqrt
import numpy as np
import matplotlib.pyplot as plt
#from statistics import stdev

plt.close('all')

M = 1000000

#--------------------------------------------------------------
# relacao de incertezas
#--------------------------------------------------------------
# pureza
u_1 = 0.0005
# [massa] repetibilidade
u_2 = 0.1/sqrt(20)
# [massa] calibracao
u_3 = 0.1/2
# [massa] reslucao
u_4 = 0.1/2
# [volume] calibracao
u_5 = 0.1
# [volume] repetibilidade
u_6 = 0.12/sqrt(16)
# [volume] temperatura
u_7 = 0.084

# estimativas
p = 0.9999 # pureza
m = 100.28 # massa
v = 100 # volume

# distribuições de entrada
dist_pureza = np.random.uniform(-u_1, u_1, M) + p
dist_m_repetibilidade = np.random.standard_t(19, M)*u_2 + m #(GL, size)
dist_m_calibracao = np.random.normal(0, u_3, M) #(media, desv_pad, size)
dist_m_resolucao = np.random.uniform(-u_4, u_4, M)
dist_v_calibracao = np.random.triangular(-u_5, 0, u_5, M) #(left, center, right, size)
dist_v_repetibilidade = np.random.standard_t(15, M)*u_6 + v
dist_v_temperatura = np.random.uniform(-u_7, u_7, M)

#print(stdev(dist_v_temperatura))

# os elementos "m" e "v" da equacao de Ccd sao compostos por 3 incertezas cada
p = dist_pureza
m = dist_m_repetibilidade + dist_m_calibracao + dist_m_resolucao
v = dist_v_calibracao + dist_v_repetibilidade + dist_v_temperatura

# distribuicao de saida segue a equacao de Ccd
y = 1000*m*p/v # 100% de confianca

# remocao de 5% dos dados
y_95 = np.sort(y)
corte = int(M*0.05/2)
y_95 = y_95[corte:len(y)-corte]

# incerteza para 95% de confianca
U = (y_95[len(y_95)-1] - y_95[0])/2

print("\n-----------------------------------------------")
print("Resultado:")
print("y = ", np.mean(y), "\u00B1", U)
print("-----------------------------------------------")

# histograma de saida 100% de confianca
plt.hist(y, 500)
plt.axvline(x=y_95[0], color = 'black', linewidth = 0.5)
plt.axvline(x=y_95[len(y_95)-1], color = 'black', linewidth = 0.5)
plt.axvline(x=np.mean(y), color = 'black', linewidth = 0.5, linestyle = "--")
plt.xticks((y_95[0], np.mean(y), y_95[len(y_95)-1]), ("-2\u03C3\n"+str(round(y_95[0],3)), round(np.mean(y), 3) , "2\u03C3\n"+str(round(y_95[len(y_95)-1],3))))
plt.arrow(np.mean(y), 500, y_95[len(y_95)-1]-np.mean(y), 0, length_includes_head=True, head_width=100, head_length=0.1, fill=True, color="black")
plt.text((np.mean(y)+y_95[len(y_95)-1])/2, 550, round(U, 3), horizontalalignment='center')